# XyGrib on Linux
Project for build and execution of XyGrib on various Linux distros using Docker. The intention is to keep an up to date list of the minimal Linux build and execution dependencies for the latest version of XyGrib. 

Tested with the release v1.2.7 of XyGrib.  

See more about XyGrib at [XyGrib on GitHub](https://github.com/opengribs/XyGrib), or at their homepage at [OpenGribs](https://opengribs.org).  


# Intro and Usage
This project requires Docker. The build is in one container, the execution in another. 

The files as they come from cmake and make build process are later present on docker host system under relevant `*_out/` folders.

The build proces writes to log files also following `*_build.log` naming pattern.

To manually test the application as a live GUI x-org or wayland would be needed on the host machine, i.e. most Linux distros for hte desktop. Alternatively the application as it is written with Qt5 can host a VNC server in the runtime container and the test user can connect over the network to the exposed port. This VNC server method is probably also the best option for testing the application when running on MacOS or Win10. See more under the under the *GUI applications and docker* topic header.  

Getting up and running:

1. The XyGrib repository should be cloned in to the same directory as this project repository for access by the docker build context.
`git clone --depth=1 https://github.com/opengribs/XyGrib.git`.
2. Next is to run the docker build and run commands. 
 - The script `xyg_docker.sh` is for automated pulling hcanges and runing builds.
 - The other `*_run.sh` scripts are dedicated for the different OS's.
3. If you wish to keep the build for permanent use on the host machine, then copy the contents of relevant `*_build/out` folder to `~/opt/XyGrib` for example.


## The dependencies
The following sections cover the dependencies for building and running XyGrib. If there are more Linux distros to add to this then perhaps raise an issue, or make a contrubution. Once this is considered completed then the following sections can be expanded upon, and perhaps integrated into the official OpenGribs/XyGrib documentation.  

Often `git` is listed as a dependency for building open source software. In this project `git` is considered an external factor, as the source code could be downloaded as a .zip for example.


### XyGrib on Debian derivatives
These are some documentations on historical builds of XyGrib.

- Version 1.2.7 XyGrib on Debian 9 Stretch and Ubuntu 18.04.2 LTS
Build: `build-essential cmake qttools5-dev qtbase5-dev libpng-dev libopenjp2-7-dev libnova-dev libproj-dev zlib1g-dev libbz2-dev`.  
Execution: `libqt5network5 libopenjp2-7 libqt5xml5 libqt5printsupport5 libproj12`.  

- Version 1.2.7 XyGrib on Debian 11
Build: `build-essential cmake qttools5-dev qtbase5-dev libpng-dev libopenjp2-7-dev libnova-dev libproj-dev zlib1g-dev libbz2-dev \`
Execution: `libqt5network5 libopenjp2-7 libqt5printsupport5 libproj19`.

Images from the [Official Ubuntu Docker images](https://hub.docker.com/_/ubuntu), and the [Official Debian Docker images](https://hub.docker.com/_/debian).


### XyGrib on Fedora 30
Base docker image `fedora:30` from the [Official Fedora Docker images](https://hub.docker.com/_/fedora).  

Build: `gcc cmake make qt5-devel libpng-devel openjpeg2-devel libnova-devel proj-devel zlib-devel bzip2-devel`.  
Execution: `libnova openjpeg2 qt5-qtbase qt5-qtbase-gui proj`.  

The dependency on libproj `libproj.so.13` is only available for Rawhide and Fedora 30. [See more](https://pkgs.org/download/libproj.so.13).


## Build and install script
The build and install processes in this project are simply the steps found at [OpenGrib's XyGrib build instructions](https://github.com/opengribs/XyGrib/blob/master/INSTALL.md#linux-1).  



# Data folder
The `data` folder should be in the folder where the XyGrib executable is located. Alternatively, when launching XyGrib then a relative path can be used. So `./opt/XyGrib/XyGrib` will work as long as `data` folder is in same directory as that command is executed in.  

The `data` folder contains the following sub-folders:  
* `colors` with 23 items, 7.1 kB - color configuration files graphically displaying different GRIB data layers.
* `fonts` with 18 items, 1.6 MB - folder where the [Liberation Fonts](https://en.wikipedia.org/wiki/Liberation_fonts) are kept, they are free replacements for the standard Microsoft proprietary fonts.
* `gis` with 6 items, 1.1 MB - cities, countries and other GIS data
* `img` with 56 items, 139.8 kB - all the icons for the application.
* `maps` with 18 items, 23.9 MB - the XyGrib base maps. These can be updated to high resolution maps (98.5MB) at [OpenGribs downloads](https://opengribs.org/en/downloads).
* `tr` with 25 items, 2.9 MB - language files for Qt5LinguistTools

# GUI applications and docker
This can be done with X Server, but there is also X11docker which is safer as it launches a second X11 server and the docker container uses that, in the interest of sandboxing. It's also possible to have a virtual desktop over TCP using VNC, and works even if the docker host machine has no display on it for example.  

See more of the ins and outs of [GUI and docker](https://wiki.ros.org/docker/Tutorials/GUI).  

## Non-safe X server method: 
The xhost on the host machine will have access control restricted. There are various ways to let a docker container connect to the xhost, some are safer than others but generally it seems to never be a great idea let the application GUI use the same X server as the host machine's default one: `xhost +local:root` and then, once done testing, return value to the normal safe one: `xhost -local:root`. Better is to only allow a specific container to access the x server as follows:  
```
xhost +local:`docker inspect --format='{{ .Config.Hostname }}' $containerId`
docker start $containerId
```
For the purposes of testing the functioning of XyGrib the unsafe method by enabling all local access to xhost could be considered alright. 


# Things to do
- Web-hook request on changes or set up a polling cron to monitor origin master at upstream for changes and then run automated build. The testing process of the GUI remains manual, but any evident dependency changes should become immediately apparent.
- Build binary software distribution packages for Flatpak, Debian, RPM etc.
- Modify script to be generic and take variables for the OS versions and build and run from this.




# License
Distributed under the GNU GENERAL PUBLIC LICENSE Version 3, see LICENSE for more information.  

Lastly, this project is independent of, and not affiliated with OpenGribs or XyGrib.
