#!/bin/bash
#
# xyg_docker.sh
#
# XyGrib build and run CI/CD workflow.
#
# For testing purposes the GUI application is run with the Qt5 VNC server mode,
# so connecting and checking the operation of the application can be done with a VNC client
# such as vinagre for example. Using VNC for testing was the easiest mode of operations as many host systems are now
# either X11 / X-org or on Wayland and it is not always simple to share the required sockets from the host's display server
# to the runtime container. 
#
# Availalbe flag is for eample: ./auto_docker.sh force
# This will force the build of images and run of containers even without changes to upstream repo.
#
# Script requirements:
#   bash
#   docker
#   git
#
# @author       Weleoka <weleoka@mailfence.com>
# @copyright    2021 
# @date         2021-10-18
#
# 🌈🔆🍀
#
# ------------------------------------------------------------------------------

# Bash strict mode
set -euo pipefail

# Colouring of feedback
ACTION='\033[1;90m'
FINISHED='\033[1;96m'
READY='\033[1;92m'
NOCOLOR='\033[0m' # No Color
ERROR='\033[0;31m'

# Absolute name and then path of this script
script="`realpath "$0"`"
SCRIPTDIR="`dirname "$script"`"

# Vars
# Absolute path to repository dir with source code.
REPODIR="${SCRIPTDIR}/XyGrib"
# Runs build process even without chages to upstream.
FORCE_RUN=${1-"disabled"}
# If upstream has new commits to master then changes is true.
CHANGES=false

# ========== FLAGS parsing ========
if [ "$FORCE_RUN" == "force" ]; then
  echo -e "${NOCOLOR}Force run enabled, will build and run docker process even without changes to repo."
  FORCE_RUN=true
fi

# ========== BEGIN logic ==========
# Check the XyGrib repository
echo -e "${ACTION}Checking Git repo"

if ! [ -d ${REPODIR} ]; then
  echo "Directory '${REPODIR}' is not valid"
  echo "Please run git clone --depth=1 https://github.com/opengribs/XyGrib.git. Aborting." >&2
  # git clone --depth=1 https://github.com/opengribs/XyGrib.git
  exit 1
else
  echo "Repository directory directory: '${REPODIR}' found."
fi

cd "${REPODIR}"
BRANCH=$(git rev-parse --abbrev-ref HEAD)
if [ "$BRANCH" != "master" ]
then
  echo -e "${ERROR}Not on master. Aborting. ${NOCOLOR}" >&2
  # git checkout master
  exit 1
else
  echo "Current working branch is master"
fi
git remote update
#git fetch
HEADHASH=$(git rev-parse HEAD)
UPSTREAMHASH=$(git rev-parse master@{upstream})
if [ "$HEADHASH" != "$UPSTREAMHASH" ]; then
  echo -e "${ACTION}Detected changes to upstream master!"
  CHANGES=true
  git pull
else
  echo -e "${FINISHED}No changes to upstream master, nothing to do."
fi
cd ..


if [ "$CHANGES" = true ] || [ "$FORCE_RUN" = true ]; then
  ./debian_run.sh
fi

exit 0