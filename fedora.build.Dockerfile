ARG FEDORA_VERSION
FROM fedora:${FEDORA_VERSION}

RUN yum install -y gcc cmake make qt5-devel libpng-devel openjpeg2-devel libnova-devel proj-devel zlib-devel bzip2-devel
WORKDIR /home/builder

 
