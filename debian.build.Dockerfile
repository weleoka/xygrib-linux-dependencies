ARG DEBIAN_VERSION
FROM debian:${DEBIAN_VERSION}

RUN apt update && apt upgrade -y

# Install dependencies then clean up apt cahce
RUN apt-get install -y \
build-essential cmake qttools5-dev qtbase5-dev libpng-dev libopenjp2-7-dev libnova-dev libproj-dev zlib1g-dev libbz2-dev \
&& rm -rf /var/lib/apt/lists/* 

WORKDIR /home/builder

COPY ./debian.entrypoint.sh /home/builder/entrypoint.sh

COPY ./XyGrib /home/builder/XyGrib

ENTRYPOINT ["./entrypoint.sh"]