ARG UBUNTU_VERSION
FROM ubuntu:${UBUNTU_VERSION}
#FROM ubuntu:18.04

RUN apt update
RUN apt-get install -y build-essential cmake qttools5-dev qtbase5-dev libpng-dev libopenjp2-7-dev libnova-dev libproj-dev zlib1g-dev libbz2-dev
WORKDIR /home/builder

