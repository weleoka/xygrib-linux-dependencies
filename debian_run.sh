#!/bin/bash

# Bash strict mode
set -euo pipefail

# Colouring of feedback
ACTION='\033[1;90m'
FINISHED='\033[1;96m'
READY='\033[1;92m'
NOCOLOR='\033[0m' # No Color
ERROR='\033[0;31m'

# Absolute name and then path of this script
script="`realpath "$0"`"
SCRIPTDIR="`dirname "$script"`"

# Make sure pwd is this scrip's dir.
cd "${SCRIPTDIR}"


# ========== BEGIN logic build ==========

# Build image with build time XyGrib dependencies
echo -e "${ACTION}Creating build docker image"
docker build \
--network "host" \
--file "./debian.build.Dockerfile" \
--tag "xyg-debian-build" \
--build-arg "DEBIAN_VERSION=11" .
echo -e "${FINISHED}Done."

# Remove old build
rm -r "./debian_out" || true
mkdir "./debian_out"

# Run the build process in build container
echo -e "${ACTION}Running build process in build container"
echo -e "Logs written to file: debian/build.log"
docker run --rm \
--name "xyg-builder" \
--volume "${SCRIPTDIR}/debian_out:/home/builder/_install/XyGrib" \
xyg-debian-build > debian_build.log 2>&1

echo -e "${FINISHED}Done!"


# ========== BEGIN logic runs ==========

# Build the application runtime image
echo -e "${ACTION}Creating runtime docker image"
docker build \
--network "host" \
--file "./debian.run.Dockerfile" \
--tag "xyg-debian-run" \
--build-arg "DEBIAN_VERSION=11" .

# Run the application in the runtime container
echo -e "${ACTION}Running application"
# Host system running X11 / Xorg
# docker run -it --rm --network "host" --name "xyg-runner" \
# -e XDG_RUNTIME_DIR=/tmp \
# -e WAYLAND_DISPLAY=$WAYLAND_DISPLAY \
# -v $XDG_RUNTIME_DIR/$WAYLAND_DISPLAY:/tmp/$WAYLAND_DISPLAY \
# -v "/home/user/00repos/xygrib-linux-dependencies/XyGribOut:/home/runner" \
# --user=$(id -u):$(id -g) \
# xyg-debian-run ./XyGrib

# Host system running Wayland
docker run -it --rm \
--network "host" \
--name "xyg-runner" \
-e QT_QPA_PLATFORM="vnc" \
-v "${SCRIPTDIR}/debian_out:/home/runner" \
xyg-debian-run ./XyGrib

echo -e "${FINISHED}XyGrib should be up and running and a Qt5 VNC server listening on port 5900"
echo -e "${FINISHED}Try connecting to the VNC server with a VNC client: vinagre localhost:5900"

exit 0