#!/bin/bash
# Script which automates the process as described at XyGrib build instructions.

echo "Building XyGrib"
cd XyGrib
rm -r build
mkdir -p build
cd build
cmake ..
make

echo "Installing XyGrib"
cmake .. -DCMAKE_INSTALL_PREFIX=../../_install
make
make install
