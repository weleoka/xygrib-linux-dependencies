ARG DEBIAN_VERSION
FROM debian:${DEBIAN_VERSION}
RUN apt update && apt upgrade -y

# Debian 9 dependencies:
#RUN apt-get install -y libqt5network5 libopenjp2-7 libqt5xml5 libqt5printsupport5 libproj12

# Debian 11 dependencies:
RUN apt-get install -y libqt5network5 libopenjp2-7 libqt5printsupport5 libproj19

WORKDIR /home/runner

COPY ./debian_out/* /home/runner/

ENTRYPOINT ["./XyGrib"]

